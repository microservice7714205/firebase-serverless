import { Request, Response } from "express";
import { Endpoint, RequestType } from "firebase-backend";

export default new Endpoint(
  "addPaymentMethod",
  RequestType.POST,
  (request: Request, response: Response) => {
    const { cardNumber, cardHolder } = request.body;

    let paymetnToken = `${cardNumber}_${cardHolder}`;
    return response.status(201).send({
      token: paymetnToken,
    });
  }
);
